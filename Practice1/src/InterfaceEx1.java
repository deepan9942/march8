@FunctionalInterface
interface i
{
	public void disp();
	static void main()
	{
		System.out.println("Static method inside functional interface");
	}
}

public class InterfaceEx1 
{

	public static void main(String[] args) 
	{
		i.main();
		i obj=()->System.out.println("Hai");
		obj.disp();
	}

	}
	
